# Services

resource "kubernetes_service" "wp_svc_mysql" {
  metadata {
    name = "wp-svc-mysql"
  }
  spec {
    selector = {
      app = var.mysql_label
    }
    port {
      port = var.mysql_port
    }
    cluster_ip = "None"
  }
}

resource "kubernetes_service" "wp_svc_wordpress" {
  metadata {
    name = "wp-svc-wordpress"
  }
  spec {
    selector = {
      app = var.wp_label
    }
    port {
      port = var.wp_port
    }
    cluster_ip = "None"
  }
}