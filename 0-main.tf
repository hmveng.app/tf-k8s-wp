provider "kubernetes" {
  config_path = "~/.kube/config"
}


#Secret

resource "kubernetes_secret" "mysql" {
  metadata {
    name = "mysql-pwd"
  }

  data = {
    password = var.mysql_pwd
  }
}