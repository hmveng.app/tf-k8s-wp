#deployment

resource "kubernetes_deployment" "wp_depl_wordpress" {
  metadata {
    name = "wp-depl-wordpress"
    labels = {
      app = var.wp_label
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = var.wp_label
      }
    }

    template {
      metadata {
        labels = {
          app = var.wp_label
        }
      }

      spec {
        container {
          image = var.wp_image
          name  = "wordpress"
          env {
            name  = "WORDPRESS_DB_HOST"
            value = kubernetes_service.wp_svc_mysql.metadata[0].name
          }

          env {
            name = "WORDPRESS_DB_PASSWORD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.mysql.metadata[0].name
                key  = "password"
              }
            }
          }

          port {
            container_port = var.wp_port
            name           = "wordpress"
          }
          volume_mount {
            name       = "wordpress-persistent-storage"
            mount_path = "/var/www/html"
          }
        }
        volume {
          name = "wordpress-persistent-storage"
          persistent_volume_claim {
            claim_name = module.pv_pvc_wordpress.pvc_name
          }
        }

      }
    }
  }
}