# PV
resource "kubernetes_persistent_volume" "wp_pv" {
  metadata {
    name = "pv-${var.pv_pvc_name}"
  }

  spec {
    capacity = {
      storage = "2Gi"
    }
    access_modes = ["ReadWriteMany"]
    persistent_volume_source {
      host_path {
        path = "${path.cwd}/${var.pv_pvc_dir}"
        type = "DirectoryOrCreate"
      }
    }
  }
}

#PVC

resource "kubernetes_persistent_volume_claim" "wp_pvc" {
  metadata {
    name = "pvc-${var.pv_pvc_name}"
  }

  spec {
    access_modes = ["ReadWriteMany"]
    resources {
      requests = {
        storage = "2Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.wp_pv.metadata[0].name
  }
}
