# get incommings and route into pod


# Ingress

resource "kubernetes_ingress_v1" "wp_ingress" {
  metadata {
    name = "wp-ingress"
    annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/"
    }
  }

  spec {
    rule {
      host = var.wp_host
      http {
        path {
          backend {
            service {
              name = kubernetes_service.wp_svc_wordpress.metadata[0].name
              port {
                number = var.wp_port
              }
            }
          }

          path = "/"
        }
      }
    }
  }
}