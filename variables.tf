variable "mysql_image" {
  type = string
}

variable "mysql_pwd" {
  type = string
}

variable "mysql_dir" {
  type = string
}

variable "mysql_label" {
  type = string
}

variable "mysql_port" {
  type = number
}

variable "wp_image" {
  type = string
}

variable "wp_port" {
  type = number
}

variable "wp_host" {
  type = string
}

variable "wp_dir" {
  type = string
}
variable "wp_label" {
  type = string
}