module "pv_pvc_mysql" {
  source      = "./modules/pv-pvc"
  pv_pvc_dir  = var.mysql_dir
  pv_pvc_name = "mysql"
}

module "pv_pvc_wordpress" {
  source      = "./modules/pv-pvc"
  pv_pvc_dir  = var.wp_dir
  pv_pvc_name = "wordpress"
}