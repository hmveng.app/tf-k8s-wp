output "app" {
  value = {
    url = kubernetes_ingress_v1.wp_ingress.spec[0].rule[0].host
  }
}