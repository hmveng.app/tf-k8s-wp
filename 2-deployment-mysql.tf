
# Deployment

resource "kubernetes_deployment" "wp_depl_mysql" {
  metadata {
    name = "wp-depl-mysql"
    labels = {
      app = var.mysql_label
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = var.mysql_label
      }
    }

    template {
      metadata {
        labels = {
          app = var.mysql_label
        }
      }

      spec {
        container {
          image = var.mysql_image
          name  = "wp-mysql"
          env {
            name = "MYSQL_ROOT_PASSWORD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.mysql.metadata.0.name
                key  = "password"
              }
            }
          }
          port {
            container_port = var.mysql_port
          }
          volume_mount {
            name       = "mysql-persistent-storage"
            mount_path = "/var/lib/mysql"
          }
        }
        volume {
          name = "mysql-persistent-storage"
          persistent_volume_claim {
            claim_name = module.pv_pvc_mysql.pvc_name
          }
        }
      }
    }
  }
}